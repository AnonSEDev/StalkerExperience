![Banner](banner.png)

# Stalker Experience

**Experience** aims to create a gameplay that revolves around long expedition favoring
stealth and tactics over direct comfrontation with anything that player stumbles against
during his raids. The player, with the help of some npcs, either leading them or just
being one more of the group, must find rare artifacts and return to sell them.
Right now **Experience** requires Anomaly, but in the future it should be game agnostic.

[Task List](./docs/TODO.md)

## Init
```cmd
dir StalkerExperience
se.cmd
# Or simply double click se.cmd
```


## Community
[General Thread](https://boards.4channel.org/vg/catalog#s=stalker)


## Structure
- *utils/* 
	- *cygwin-portable/:* bash,wget... scripting stuff
- *scripts/:* SE related scripts
	- *base/:* Actual stalker scripts files
    - *dbutil.sh": Unpack/Pack .db files
    - *patch.sh": Edit script/config files
- *base/:* Original scripts that this project modifies


## Management tool
Includes gui and shell tool.

![Banner](docs/gui_tool.png)


## Base
Anomaly
https://www.moddb.com/mods/stalker-anomaly/downloads/stalker-anomaly-151

https://bitbucket.org/anomalymod/xray-monolith/src/master/


## Tools
(Un)Packer
https://www.stalker-anomaly.com/2019/03/07/modding-stalker-anomaly/

xrCompress
https://xray-engine.org/index.php?title=xrCompress

Cygwin portable
https://vegardit.github.io/cygwin-portable-installer/

ACDC
https://www.moddb.com/mods/call-of-chernobyl/downloads/acdc-tool-for-coc


## Scripting Links
http://sdk.stalker-game.com/en/index.php?title=S.T.A.L.K.E.R._MOD_portal

https://www.stalker-anomaly.com/2019/03/07/modding-stalker-anomaly/

https://www.moddb.com/mods/stalker-wormwood/tutorials/creating-your-first-stalker-call-of-pripyat-mod

https://springrts.com/wiki/Lua_Performance


## License
[GPL2](https://choosealicense.com/licenses/gpl-2.0/)