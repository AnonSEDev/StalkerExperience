# Anomaly Engine Annotations

Documentation and findings about stalker engine for reference


## Files

- Stalker uses two directories for loading scripts:
    - db/: Packed scripts (unpack with converter.exe)
    - gamedata/: Unpacked scripts (crashes if you put .db there).
    
    I assume db is loaded first.
    
- **.ltx**: Simple config (INI) files, for [example](https://github.com/kasperschipper/stalkeranomaly_mods/blob/main/_unpacked/configs/alife.ltx). Most of them reside in config/

- **spawns/all.spawn** contains data about npcs/items/anomalies on new game startup. Needs acdc tool to
    compile or decompile. Contains three kind of files:
    - **.section**: No idea what they are, tried truncating one and had no effect
    - **.bin**: It seems to contains .section files
    - **.ltx**:
        - Two kind of them
            - Actual spawn data (alife_l02_garbage.ltx)
            - Waypoints data (way_l02_garbage.ltx)
    
        - The files need to exist. You can empty them to disable spawns (Maybe editing all.ltx I can bypass this).
        - **alife_fake_start.ltx**: Where the player spawns
        - **alife_l01_escape.ltx**: Cordon (player start point)

    Some links:

    [Edit all.spawn](http://sdk.stalker-game.com/en/index.php?title=Editing_all.spawn)

    [Working on all.spawn with SOC level editor](https://www.moddb.com/games/stalker/tutorials/working-on-allspawn-with-soc-level-editor)
    
- [configs/misc/simulation.ltx](https://github.com/kasperschipper/stalkeranomaly_mods/blob/main/_unpacked/configs/misc/simulation.ltx) npc spawns

## Scripting

- Lua function bindings 
- Places where the engine registers functions https://bitbucket.org/search?q=repo%3Axray-monolith%20script_register&account=%7Be02f84e0-88d5-4df3-b8a6-d9054782709d%7D
    - (c++) XML Loader https://bitbucket.org/anomalymod/xray-monolith/src/master/src/xrXMLParser/xrXMLParser.cpp
    - (lua) XML Utils https://github.com/kasperschipper/stalkeranomaly_mods/blob/main/_unpacked/scripts/utils_xml.script
    
- [API](https://github.com/kasperschipper/stalkeranomaly_mods/blob/main/_unpacked/scripts/axr_main.script)
    - Scripts that bridges between the engine and lua usually starts with 'binder', for example:
        - [Bind stalker](https://github.com/kasperschipper/stalkeranomaly_mods/blob/main/_unpacked/scripts/bind_stalker.script)
        - [Bind campfire](https://github.com/kasperschipper/stalkeranomaly_mods/blob/main/_unpacked/scripts/bind_campfire.script)
        
    - [Engine -> Lua](https://github.com/kasperschipper/stalkeranomaly_mods/blob/main/_unpacked/scripts/lua_help.script)
    
### GUI

- [Main GUI](https://github.com/kasperschipper/stalkeranomaly_mods/blob/main/_unpacked/scripts/ui_main_menu.script)
    - [Engine](https://bitbucket.org/anomalymod/xray-monolith/src/master/src/xrGame/MainMenu.cpp)
    - [Layout](https://github.com/kasperschipper/stalkeranomaly_mods/blob/main/_unpacked/configs/ui/ui_mm_main_16.xml)
    - [Text (depends on language)](https://github.com/kasperschipper/stalkeranomaly_mods/blob/main/_unpacked/configs/text/eng/ui_st_mm.xml)
    - [Engine init xml here](https://bitbucket.org/anomalymod/xray-monolith/src/master/src/xrGame/ui/UIMMShniaga.cpp)

- [New game GUI](https://github.com/kasperschipper/stalkeranomaly_mods/blob/main/_unpacked/scripts/ui_mm_faction_select.script)


### db.actor

Player variable. I see many null checks, I assume it is set to null if player dies?


## XRay Engine

