@echo off

:: Adapted from https://vegardit.github.io/cygwin-portable-installer/

set CYGWIN_MIRROR=https://linux.rz.ruhr-uni-bochum.de/download/cygwin
set CYGWIN_ARCH=auto
set CYGWIN_USERNAME=root
set CYGWIN_PACKAGES=yad,xinit,patch,ca-certificates,gnupg,libiconv,wget,dos2unix,procps-ng
set "CYGWIN_PATH=%%SystemRoot%%\system32;%%SystemRoot%%"
set "INSTALL_ROOT=%~dp0"
set "CYGWIN_ROOT=%INSTALL_ROOT%cygwin"

if not exist "%CYGWIN_ROOT%" (
  echo Creating Cygwin root [%CYGWIN_ROOT%]...
  md "%CYGWIN_ROOT%" || goto :fail
) else (
  echo Granting user [%USERNAME%] full access to Cygwin root [%CYGWIN_ROOT%]...
  icacls "%CYGWIN_ROOT%" /T /grant "%USERNAME%:(CI)(OI)(F)"
)

:: https://blogs.msdn.microsoft.com/david.wang/2006/03/27/howto-detect-process-bitness/
if "%PROCESSOR_ARCHITECTURE%" == "x86" (
    if defined PROCESSOR_ARCHITEW6432 (
      set CYGWIN_ARCH=64
    ) else (
      set CYGWIN_ARCH=32
    )
)

:: download Cygwin 32 or 64 setup exe depending on detected architecture
if "%CYGWIN_ARCH%" == "64" (
  set CYGWIN_SETUP_EXE=setup-x86_64.exe
) else (
  set CYGWIN_SETUP_EXE=setup-x86.exe
)
call :download "https://cygwin.org/%CYGWIN_SETUP_EXE%" "%CYGWIN_ROOT%\%CYGWIN_SETUP_EXE%"

"%CYGWIN_ROOT%\%CYGWIN_SETUP_EXE%" --no-admin ^
  --site %CYGWIN_MIRROR% ^
  --root "%CYGWIN_ROOT%" ^
  --local-package-dir "%CYGWIN_ROOT%\.pkg-cache" ^
  --no-shortcuts ^
  --no-desktop ^
  --delete-orphans ^
  --upgrade-also ^
  --no-replaceonreboot ^
  --quiet-mode ^
  --packages %CYGWIN_PACKAGES% || goto :fail

rd /s /q "%CYGWIN_ROOT%\.pkg-cache"

set "Cygwin_bat=%CYGWIN_ROOT%\Cygwin.bat"
if exist "%Cygwin_bat%" (
  echo Disabling default Cygwin launcher [%Cygwin_bat%]...
  if exist "%Cygwin_bat%.disabled" (
    del "%Cygwin_bat%.disabled" || goto :fail
  )
  rename "%Cygwin_bat%" Cygwin.bat.disabled || goto :fail
)

set "Init_sh=%CYGWIN_ROOT%\portable-init.sh"
echo Creating [%Init_sh%]...
(
  echo #!/usr/bin/env bash
  echo.
  echo #
  echo # Map Current Windows User to root user
  echo #
  echo.
  echo # Check if current Windows user is in /etc/passwd
  echo USER_SID="$(mkpasswd -c | cut -d':' -f 5)"
  echo if ! grep -F "$USER_SID" /etc/passwd ^&^>/dev/null; then
  echo   echo "Mapping Windows user '$USER_SID' to cygwin '$USERNAME' in /etc/passwd..."
  echo   GID="$(mkpasswd -c | cut -d':' -f 4)"
  echo   echo $USERNAME:unused:1001:$GID:$USER_SID:$HOME:/bin/bash ^>^> /etc/passwd
  echo fi
  echo.
  echo cp -rnv /etc/skel/. /home/$USERNAME
  echo.
  echo #
  echo # adjust Cygwin packages cache path
  echo #
  echo pkg_cache_dir=$(cygpath -w "$CYGWIN_ROOT/.pkg-cache"^)
  echo # not using "sed -i" to prevent "sed: preserving permissions for ‘/etc/setup/sedVf6T9x’: Permission denied"
  echo sed -E "s/.*\\\cygwin-pkg-cache/"$'\t'"${pkg_cache_dir//\\/\\\\}/" /etc/setup/setup.rc ^> /etc/setup/setup.rc.mod
  echo mv -f /etc/setup/setup.rc.mod /etc/setup/setup.rc
  echo.
  echo #
  echo # Make python3 available as python if python2 is not installed
  echo #
  echo python3=$(/usr/bin/find /usr/bin -maxdepth 1 -name "python3.*" -print -quit ^| head -1^)
  echo if [[ -e $python3 ]] ^> /dev/null; then
  echo   [[ -e /usr/bin/python3 ]] ^|^| /usr/sbin/update-alternatives --install /usr/bin/python3 python3 "$python3" 1
  echo   [[ -e /usr/bin/python  ]] ^|^| /usr/sbin/update-alternatives --install /usr/bin/python  python  "$python3" 1
  echo fi
) >"%Init_sh%" || goto :fail
"%CYGWIN_ROOT%\bin\dos2unix" "%Init_sh%" || goto :fail

set "Start_cmd=%INSTALL_ROOT%cygwin-portable.cmd"
echo Creating launcher [%Start_cmd%]...
(
  echo @echo off
  echo setlocal enabledelayedexpansion
  echo set "CWD=%%cd%%"
  echo set CYGWIN_DRIVE=%%~d0
  echo set "CYGWIN_ROOT=%%~dp0cygwin"
  echo.
  echo set "CYGWIN_PATH=%CYGWIN_PATH%;%%CYGWIN_ROOT%%\bin"
  echo set "PATH=%%CYGWIN_PATH%%"
  echo.
  echo set "ALLUSERSPROFILE=%%CYGWIN_ROOT%%\.ProgramData"
  echo set "ProgramData=%%ALLUSERSPROFILE%%"
  echo set CYGWIN=nodosfilewarning
  echo.
  echo set "USERNAME=%CYGWIN_USERNAME%"
  echo set "HOME=/home/%%USERNAME%%"
  echo set SHELL=/bin/bash
  echo set HOMEDRIVE=%%CYGWIN_DRIVE%%
  echo set "HOMEPATH=%%~p0cygwin\home\%%USERNAME%%"
  echo set GROUP=None
  echo set GRP=
  echo.
  echo ::echo Replacing [/etc/fstab]...
  echo ^(
  echo   echo # /etc/fstab
  echo   echo # IMPORTANT: this files is recreated on each start by cygwin-portable.cmd
  echo   echo #
  echo   echo #    This file is read once by the first process in a Cygwin process tree.
  echo   echo #    To pick up changes, restart all Cygwin processes.  For a description
  echo   echo #    see https://cygwin.com/cygwin-ug-net/using.html#mount-table
  echo   echo.
  echo   echo # noacl = disable Cygwin's - apparently broken - special ACL treatment which prevents apt-cyg and other programs from working
  echo   echo none /cygdrive cygdrive binary,noacl,posix=0,user 0 0
  echo ^) ^> "%%CYGWIN_ROOT%%\etc\fstab"
  echo.
  echo %%CYGWIN_DRIVE%%
  echo chdir "%%CYGWIN_ROOT%%\bin"
  echo ::echo Loading [%%CYGWIN_ROOT%%\portable-init.sh]...
  echo bash "%%CYGWIN_ROOT%%\portable-init.sh"
  echo.
    :: https://stackoverflow.com/a/8452363/5116073
  echo setlocal EnableDelayedExpansion
  echo bash --login -c %%*
  echo.
  echo cd "%%CWD%%"
) >"%Start_cmd%" || goto :fail

:: launching Bash once to initialize user home dir
call "%Start_cmd%" "whoami"
set "Bashrc_sh=%CYGWIN_ROOT%\home\%CYGWIN_USERNAME%\.bashrc"

:: apt-cyg
call "%Start_cmd%" "wget -O /usr/local/bin/apt-cyg https://raw.githubusercontent.com/kou1okada/apt-cyg/master/apt-cyg"
call "%Start_cmd%" "chmod +x /usr/local/bin/apt-cyg"

:: winpty
if "%CYGWIN_ARCH%" == "64" (
    set winpty_download_url=https://github.com/rprichard/winpty/releases/download/0.4.3/winpty-0.4.3-cygwin-2.8.0-x64.tar.gz
) else (
    set winpty_download_url=https://github.com/rprichard/winpty/releases/download/0.4.3/winpty-0.4.3-cygwin-2.8.0-ia32.tar.gz
)
call "%Start_cmd%" "wget -qO- --show-progress %winpty_download_url% ^| /usr/bin/tar xzv -C / --strip-components 1"

:: remove stuff
call "%Start_cmd%" "apt-cyg remove groff man-db libllvm8"
rd /s /q "%CYGWIN_ROOT%\usr\share\doc"
rd /s /q "%CYGWIN_ROOT%\usr\share\locale"
rd /s /q "%CYGWIN_ROOT%\usr\share\icons\Adwaita\cursors"
rd /s /q "%CYGWIN_ROOT%\usr\share\man"
::rd /s /q "%CYGWIN_ROOT%\bin\cygwebkitgtk-3.0-0.dll"

::
find "export PYTHONHOME" "%Bashrc_sh%" >NUL || (
  echo.
  echo export PYTHONHOME=/usr
) >>"%Bashrc_sh%" || goto :fail

"%CYGWIN_ROOT%\bin\dos2unix" "%Bashrc_sh%" || goto :fail
pause

:fail
  set exit_code=%ERRORLEVEL%
  if exist "%DOWNLOADER%" (
    del "%DOWNLOADER%"
  )
  echo # Installing [Cygwin Portable] FAILED!
  exit /B %exit_code%

:download
  if exist %2 (
    echo Deleting existing [%2]...
    del %2 || goto :fail
  )

  powershell "[Net.ServicePointManager]::SecurityProtocol = 'tls12, tls11, tls'; (New-Object Net.WebClient).DownloadFile('%1', '%2')" || exit /B 1
  exit /B 0