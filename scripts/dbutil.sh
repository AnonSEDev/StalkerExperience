#!/bin/bash
# Utils to manipulate Stalker .db and related files
# This is sourced from se.sh

# -- db utils
function unpack_db () {
    # $1 Input file/folder
    # $2 Output folder

    mkdir -p $2
    o=$(cygpath -w $2)

    # Unpack
    if [[ -d $1 ]]
    then        
        for f in $1/*.db0
        do
            f=$(cygpath -w $f)
            util/converter.exe -unpack -xdb $f -dir $o &> /dev/null
        done
    else
        f=$(cygpath -w $1)
        util/converter.exe -unpack -xdb $f -dir $o &> /dev/null
    fi
}

function pack_db () {
    # $1 Input file/folder
    # $2 Output folder
    
    cp util/dirs.txt dirs.txt
    mkdir -p $2
    o=$(cygpath -w $2)
    
    for f in $1/*
    do
        [[ $f == *"spawnsu" ]] && continue
        f=$(cygpath -w $f)
        util/xrCompress.exe $f -ltx dirs.txt > /dev/null
        mv $f.db0 $o
    done

    rm dirs.txt engine.log
}

# -- spawn utils
function acdc () {
    (
        cd util/ACDC_update_for_1.4

        # Enable local dir imports https://wiki.gentoo.org/wiki/Project:Perl/Dot-In-INC-Removal
        export PERL_USE_UNSAFE_INC=1

        # Script Portability https://perldoc.perl.org/perlcygwin#Script-Portability-on-Cygwin
        # "A file will always be treated as binary .. just like it is in UNIX. So CR/LF translation needs to be requested"
        export PERLIO=crlf

        perl universal_acdc.pl $@
    )
}

function decompile_spawn () {
    mkdir -p ../../$2
    acdc -d $1 -out ../../$2 -scan configs -sort complex -nofatal > /dev/null
}

function compile_spawn () {
    mkdir -p ../../$2
    acdc -compile $1 -out ../../$2 > /dev/null
}