#!/bin/bash
# Utils to display menus
# This is sourced from se.sh


function gui_startxwin {
    # Original cygwin/bin/startxwin

    unset DBUS_SESSION_BUS_ADDRESS
    unset SESSION_MANAGER
    export NO_AT_BRIDGE=1

    server=/usr/bin/XWin
    display=":0"
    serverargs=""

    # Automatically determine an unused $DISPLAY
    d=0
    while true ; do
        [ -e "/tmp/.X$d-lock" -o -S "/tmp/.X11-unix/X$d" ] || break
        kill -0 `cat /tmp/.X$d-lock` 2>/dev/null || break
        d=$(($d + 1))
    done
    display=":$d"
    unset d
    serverargs="-multiwindow $serverargs"
    eval \"$server\" $display $serverargs &
}

function kill_this() {
    (ps | grep $1 > /dev/null) &&
    /bin/kill -f $(ps | grep $1 | awk '{$1=$1};1' | cut -d ' ' -f 1)
}

function gui_startx () {
    (ps | grep XWin > /dev/null) && return 1
    export DISPLAY=:0.0
    gui_startxwin &
    sleep 3
    return 0
}

function gui_stopx () {
    kill_this XWin
}

function gui_call () {
    export NO_AT_BRIDGE=1
    gui_startx
    RET=$?
    yad "$@" 2>/dev/null
    r=$?
    kill_this dbus-daemon
    [[ $RET == 0 ]] && gui_stopx
    return $r
}

function gui_info () {
    gui_call --no-buttons --title="Stalker EXPERIENCE" --info --text="$@"
    return $?
}

function gui_info_time () {
    tm=$1
    shift
    gui_call --no-buttons --title="Stalker EXPERIENCE" --info --timeout $tm --text="$@"
    return $?
}

function gui_file_then () {
    # $1 Text to display
    # $2 return function
    # $3 accept function
    r=$(gui_call \
    --title="Stalker EXPERIENCE file browser" \
    --file \
    --text=$1)

    [[ $? = 0 ]] && $3
    $2
}

# -- BIG GUI
se_output="/tmp/se_gui_res"

function gui_main () {
    # -- Callbacks
    function cb {
        echo $1 > $se_output
    }
    
    export -f cb
    export se_output

    # Clear
    cb_a="sh -c 'cb 1'"
    # Unpack db
    cb_b="sh -c 'cb 2'"
    # Decompile spawn
    cb_c="sh -c 'cb 3'"
    # Compile spawn
    cb_d="sh -c 'cb 4'"
    # Pack db
    cb_e="sh -c 'cb 5'"
    # Website
    cb_h="sh -c 'cygstart https://codeberg.org/AnonSEDev/StalkerExperience'"
    
    # -- Start gui
    gui_call \
    `# -- Config` \
    --center --title="EXPERIENCE Management tool" --fixed \
    --form --no-buttons --image="$se_dir/scripts/banner_gui.png" \
    `# -- Options` \
    --field="Clean"!""!"Clear baseu/ out/ folders":FBTN "$cb_a" \
    --field="Unpack .db":FBTN "$cb_b" \
    --field="Decompile .spawn":FBTN "$cb_c" \
    --field="Compile .spawn":FBTN "$cb_d" \
    --field="Pack .db":FBTN "$cb_e" \
    --field="Web"!""!"Repository site":FBTN "$cb_h"
}

function se () {
    "$se_dir/se.sh" "$@"
}

function se_gui () {
    rm -f $se_output
    gui_startx
    gui_main &
    sleep 2
    loop=1

    while [[ $loop = 1 ]]
    do
        if ! pgrep -x "yad" > /dev/null
        then
            break
        fi

        sleep 0.1
        [[ ! -f $se_output ]] && continue

        opt=$(cat $se_output)
        [[ ! $opt == 1 ]] && kill_this yad
        
        case $opt in
            1)
                gui_info_time 3 "Cleaning folders ..." &
                se --clean
                ;;
            2)
                function payload () {
                    gui_info_time 3 "Please wait" &
                    se --unpack $r
                    gui_info_time 3 "Done"
                    cygstart "$se_dir/baseu"
                }

                gui_file_then "Select a .db0 file or a directory" se_gui payload
                ;;
            3)            
                function payload () {
                    gui_info_time 3 "Please wait" &
                    se --unpack-spawns $r
                    gui_info_time 3 "Done"
                    cygstart "$se_dir/baseu/spawnsu/"
                }

                gui_file_then "Select all.spawn file" se_gui payload
                ;;
            # Compile .spawn
            4)
                function payload () {
                    gui_info_time 3 "Please wait 1 min" &
                    se --pack-spawns $r
                    gui_info_time 3 "Done"
                    cygstart "$se_dir/baseu/spawns/"
                }

                gui_file_then "Select decompiled .spawn folder" se_gui payload
                ;;
            # Pack .db
            5)
                function payload () {
                    gui_info_time 3 "Please wait" &
                    se --pack $r
                    gui_info_time 3 "Done"
                    cygstart "$se_dir/out"
                }

                gui_file_then "Select a folder" se_gui payload
                ;;
            *)
                loop=0
                ;;
        esac
        
        rm -f $se_output
    done

    gui_stopx
}