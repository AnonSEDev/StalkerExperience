#!/bin/bash

# Actual changes to the code go here

function filter_ltx () {
    ### Sections are separated by long ;;;; lines
    ### Section names http://sdk.stalker-game.com/en/index.php?title=Call_of_Pripyat_-_Modders_Resource_List
    # $1 Path
    # $@ section_names we are interested

    p=$1
    shift
    for f in $(find $p -name '*.ltx')
    do
        [[ $f == *"fake_start"* ]] && continue
        [[ $f == *"all.ltx" ]] && continue
        [[ $f == *"way_"* ]] && continue

        buf=""
        nbuf=""
        skip=0

        function flush_buf () {
            [[ $skip == 0 ]] && nbuf="$nbuf$buf"
            buf=""
        }

        OIFS=$IFS
        IFS=$'\n'

        for line in $(cat $f)
        do
            if [[ $line == ";;;;"* ]]; then
                flush_buf
                skip=0
            fi

            if [[ $skip == 0 ]] && [[ $line == "section_name"* ]]; then
                skip=1
                for s in $@
                do
                    [[ $line == "section_name = "$s* ]] && skip=0 && break
                done
            fi

            [[ $skip == 1 ]] && continue
            [[ $line == *"\\"* ]] && line="${line//\\/\\\\}" # Fucking shit
            buf="$buf\n$line" 
        done

        IFS=$OIFS
        flush_buf

        echo -e $nbuf > $f
    done
}

function clear_spawns () {
    # Filter spawns
    filter_ltx "baseu/spawnsu" "box" "breakable_object" "camp_zone" "campfire" "door_lab_x8" "climable_object" "physic_" "inventory_box" "level_changer"
}

function rewrite_db () {
    # We use folders, but stalker/lua hates them
    mkdir -p baseu/scripts
    mkdir -p baseu/configs
    
    shopt -s globstar
    cp -R -f src/scripts/**/*.script baseu/scripts/
    cp -R -f src/configs baseu/
}

function patch_db () {
    [[ ! -n "$no_spawns" ]] && clear_spawns
    rewrite_db
}