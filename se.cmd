@echo off
:: Adapt scripts to bash
:: util\bin\dos2unix.exe -q -k **.sh

:: Setup cygwin
if not exist "util\cygwin-portable\cygwin\" (
    :: Download and install
    call util\cygwin-portable\cygwin-portable-installer.cmd
)

:: -----------------------------------------------

:: Set locale
:: https://cygwin.com/cygwin-ug-net/setup-locale.html
set LANG=C.utf8

:: Simple wrapper for our bash script
if "%~1"=="" (
    START /B msg /time:5 * "Loading, please wait. A new window will appear ..."
)
util\cygwin-portable\cygwin-portable.cmd '%CD:\=/%/se.sh "$@"' IGNORE %*